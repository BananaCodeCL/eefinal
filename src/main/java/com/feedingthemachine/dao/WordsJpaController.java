/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.dao;

import com.feedingthemachine.dao.exceptions.NonexistentEntityException;
import com.feedingthemachine.dao.exceptions.PreexistingEntityException;
import com.feedingthemachine.eefinal.entity.Words;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author feedingthemachine
 */
public class WordsJpaController implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    
    public WordsJpaController() {
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Words words) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(words);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findWords(words.getId()) != null) {
                throw new PreexistingEntityException("Words " + words + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Words words) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            words = em.merge(words);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = words.getId();
                if (findWords(id) == null) {
                    throw new NonexistentEntityException("The words with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Words words;
            try {
                words = em.getReference(Words.class, id);
                words.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The words with id " + id + " no longer exists.", enfe);
            }
            em.remove(words);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Words> findWordsEntities() {
        return findWordsEntities(true, -1, -1);
    }

    public List<Words> findWordsEntities(int maxResults, int firstResult) {
        return findWordsEntities(false, maxResults, firstResult);
    }

    private List<Words> findWordsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Words.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Words findWords(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Words.class, id);
        } finally {
            em.close();
        }
    }

    public int getWordsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Words> rt = cq.from(Words.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
