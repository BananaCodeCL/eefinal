/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.eefinal.dto;

import java.util.ArrayList;

/**
 *
 * @author feedingthemachine
 */
public class RootDto {
    private String id;
    Metadata MetadataObject;
    ArrayList<Object> results = new ArrayList<Object>();
    private String word;

    public ArrayList<Object> getResults() {
        return results;
    }

    public void setResults(ArrayList<Object> results) {
        this.results = results;
    }

    public String getId() {
      return id;
    }

    public Metadata getMetadata() {
      return MetadataObject;
    }

    public String getWord() {
      return word;
    }

   // Setter Methods 

    public void setId( String id ) {
      this.id = id;
    }

    public void setMetadata( Metadata metadataObject ) {
      this.MetadataObject = metadataObject;
    }

    public void setWord( String word ) {
      this.word = word;
    }
}

