package com.feedingthemachine;

import com.feedingthemachine.dao.WordsJpaController;
import com.feedingthemachine.eefinal.entity.Words;
import com.feedingthemachine.eefinal.dto.RootDto;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.*;
/**
 *
 * @author feedingthemachine
 */
@Path("significado")
public class AppDiccionario {
    
    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public String significado(@PathParam("palabra") String palabra){
        
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/"+ palabra);
        RootDto palabraDto = myResource.request(MediaType.APPLICATION_JSON).header("app_key", "bcc0c7cb9c39c507de7de87216af9647").header("app_id", "4f50ea39").get(RootDto.class);
        
        Words word = new Words();
        word.setPalabra(palabraDto.getWord());
        Date fecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        word.setId(new BigDecimal(Math.random()));
        word.setConsulta(dateFormat.format(fecha));
        JSONObject obj = new JSONObject(palabraDto.getResults().get(0));
        word.setSignificado(palabraDto.getResults().get(0).toString());
        WordsJpaController dao = new WordsJpaController();
        
        try {
            dao.create(word);
        } catch (Exception ex) {
            Logger.getLogger(AppDiccionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
        
        return word.getSignificado();
    }
}
